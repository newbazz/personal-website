import React from 'react'
import "./Skills.css";
import Header from "./../Header/Header.jsx";
import Footer from "./../Footer/Footer.jsx";
import skillsVector from "./../../assets/skills_vector.png";
import {skillList} from "./../../assets/skillsData";
import SkillCard from "./SkillCard.jsx";
function Skills() {
  return (
    <div className='section-container'>
      <Header
        heading="My Skills."
        subHeading="Curious techie passionate about learning. Open mindset is the greatest weapon I have in my armour.
        Here is a list of techonologies I've worked with!"></Header>
      <div className='skill-card-container'>
      {
        skillList.map(({skillName, skillUrl}, index) =>  <SkillCard key={index} skillName={skillName}
          skillUrl={skillUrl}
          />)
      }
      </div>
        <div className='skills-vector-frame'>
          <img src={skillsVector} alt="skill-vector"
          className='skills-vector' loading="lazy"
          />
        </div>
    </div>
  )
}

export default Skills