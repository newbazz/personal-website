import React from 'react'
import "./About.css";
import Header from "./../Header/Header.jsx"
import Footer from "./../Footer/Footer.jsx"
import aboutVector from "./../../assets/about_vector.png";
import aboutAnime from "./../../assets/about_anime.gif";
function About() {
  return (
    <div className='section-container'>
      <Header 
        heading="About Me"
        subHeading="Software Engineer | Blogger | Traveller | Finance Enusiast">
      </Header>
      <div className='about-main'>
        <div className='about-main-left'>
            <h3 className='about-sub-heading'>Developer</h3>
            <p className='about-sub-heading-details'>
            I am a passionate engineer, keen on solving high-impact problems. I believe that constant learning is the sole way one can survive in the tech industry where software can make or break the world.
            </p>
            <h3 className='about-sub-heading'>Blogger | Traveller | Finance Enthusiast</h3>
            <p className='about-sub-heading-details'>
            Apart from being a techie, I am also a finance enthusiast and an avid reader. I love traveling and also write blogs about random life stuff on my medium channel. I also have a book review channel on Instagram which you can go to using <a href='https://www.instagram.com/book_pre_reviewer/'>this</a> link.
            
            </p>
        </div>
        <div className='about-main-right'>
            <img 
                src={aboutAnime}
                alt="about-anime"
                className="about-anime"
                loading="lazy"
            />
        </div>
      </div>
      <Footer 
        phrase="Check out my "
        link="projects!"
        toAddress="/projects">
      </Footer>
      <div className='vector-frame'>
        <img src={aboutVector}
          alt="about"
          className='about-vector'
          loading="lazy"
        />
      </div>
    </div>
  )
}

export default About