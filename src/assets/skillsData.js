export const skillList = [
    {
        skillName: 'HTML',
        skillUrl: 'https://user-images.githubusercontent.com/31516195/170096301-84bb07bb-c807-46e4-a7d1-72e3f86c611c.png',
    },
    {
        skillName: 'CSS',
        skillUrl: 'https://user-images.githubusercontent.com/31516195/170096288-b6a601b7-7f5c-489e-be56-f9a7d61208f4.png',
    },
    {
        skillName: 'JavaScript',
        skillUrl: 'https://user-images.githubusercontent.com/31516195/170096309-ad1895af-57fc-4744-ad4c-35888c4f125b.png',
    },
    {
        skillName: 'C#',
        skillUrl: 'https://github.com/newbazz/image-repo/blob/main/C#.png',
    },
    {
        skillName: 'Azure',
        skillUrl: 'https://github.com/newbazz/image-repo/blob/main/C#.png',
    }
];
